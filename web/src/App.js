import history from './history';
import { Router as BrowserRouter, Switch, Route } from 'react-router-dom';
import layout from './common/component/layout';

function App() {
  return (
    <BrowserRouter history={history} basename={"/"} >
      <div>
        <Switch>
          <Route component={layout} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;