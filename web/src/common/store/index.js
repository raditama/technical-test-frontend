import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { combineReducers } from 'redux';
import general from './general/reducer'

const rootReducer = combineReducers({
    general
});

export const store = createStore(
    rootReducer,
    applyMiddleware(
        thunkMiddleware
    )
);