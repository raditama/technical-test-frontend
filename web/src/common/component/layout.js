import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Switch, Route } from 'react-router-dom';

import Header from './header';
import AddEvent from '../../app/add-event/add-event';
import Dashboard from '../../app/dashboard/dashboard';
import '../asset/css/styles.css';
import Home from '../../app/home/home';

class Layout extends Component {
    render() {
        return (
            <div>
                <Header path={this.props.location.pathname} />
                <div>
                    <Switch>
                        <Route exact path={"/"} component={Home} />
                        <Route exact path={"/add-event"} component={AddEvent} />
                        <Route exact path={"/dashboard"} component={Dashboard} />
                    </Switch>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {

    };
}

const mapDispatchToProps = (dispatch) => {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Layout);