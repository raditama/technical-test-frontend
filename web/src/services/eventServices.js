import Axios from 'axios';
import { DEV_URL } from '../common/constants';

const EventServices = {
    create(data) {
        return Axios.request({
            method: 'post',
            url: `${DEV_URL}/event/create`,
            data,
        })
    },
    read() {
        return Axios.request({
            method: 'get',
            url: `${DEV_URL}/event/read`,
        })
    },
}

export default EventServices;