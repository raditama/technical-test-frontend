import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import '../../common/asset/css/styles.css';
import EventServices from '../../services/eventServices';

const Home = (props) => {
    const [list, setList] = useState([]);

    useEffect(() => {
        EventServices.read().then(res => {
            console.log(res);
            if (res.data !== '') {
                setList(res.data);
            }
        }).catch(err => {
            console.log(err);
            setList([]);
        })
    }, [])

    return (
        <div className='container_home'>
            {list?.map((data, index) =>
                <div className='card_home' key={index}>
                    <div className='container_img'>
                        <div className='img' style={{backgroundImage: `url(${data?.image})`}} />
                    </div>

                    <p>{data?.location}</p>
                    <h2>{data?.title}</h2>
                    <p className='date'>
                        {moment(data?.date).format("DD MMMM YY")}
                    </p>

                    <div className='line' />
                    <p>{data?.participant}</p>
                    <div className='line' />

                    <p>Note: </p>
                    <p>{data?.note}</p>
                </div>
            )}
        </div>
    )
}

const mapStateToProps = state => ({

});

const mapDispatchToProps = (dispatch => ({

}))();

export default connect(mapStateToProps, mapDispatchToProps)(Home);