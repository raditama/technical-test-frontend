import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import '../../common/asset/css/styles.css';
import EventServices from '../../services/eventServices';
import swal from 'sweetalert';

const AddEvent = (props) => {
    const [title, setTitle] = useState('');
    const [location, setLocation] = useState('');
    const [participant, setParticipant] = useState('');
    const [date, setDate] = useState('');
    const [note, setNote] = useState('');
    const [image, setImage] = useState('');

    const _submitHandler = () => {
        if (note.length > 50
            && title !== ''
            && location !== ''
            && participant !== ''
            && date !== ''
            && image !== '') {
            const data = {
                title,
                location,
                participant,
                date,
                note,
                image,
            }

            EventServices.create(data).then(res => {
                console.log(res);
                swal("Good job!", "Successfully added event", "success");
            }).catch(err => {
                console.log(err);
                swal('F A I L E D !');
            })
        } else {
            var msg = ""
            if (title === '') {
                msg += "Please enter your title!\n"
            }
            if (location === '') {
                msg += "Please enter your location!\n"
            }
            if (participant === '') {
                msg += "Please enter your participant!\n"
            }
            if (date === '') {
                msg += "Please enter your date!\n"
            }
            if (note.length <= 50) {
                msg += "Please enter a valid note!\n"
            }
            if (image === '') {
                msg += "Please upload your image!\n"
            }
            swal('W A R N I N G !', msg);
        }
    }

    useEffect(() => {
        setTitle(localStorage.getItem('title'));
        setLocation(localStorage.getItem('location'));
        setParticipant(localStorage.getItem('participant'));
        setDate(localStorage.getItem('date'));
        setNote(localStorage.getItem('note'));
    }, [])

    useEffect(() => {
        localStorage.setItem('title', title);
        localStorage.setItem('location', location);
        localStorage.setItem('participant', participant);
        localStorage.setItem('date', date);
        localStorage.setItem('note', note);
    }, [title, location, participant, date, note])

    const changeHandler = (event) => {
        const file = event.target.files[0];

        let document = "";
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            document = reader.result;
            setImage(document);
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
	};

    return (
        <div className='wrapper_add'>
            <div className='container_add'>
                <div className='container_left'>
                    <h3>+Add Event</h3>
                    <div className='row_input'>
                        <div className='col_input'>
                            <p>Title *</p>
                            <input
                                placeholder='Input title...'
                                spellCheck='false'
                                onChange={(e) => setTitle(e.target.value)}
                                value={title}
                            />
                        </div>
                        <div className='col_input'>
                            <p>Location *</p>
                            <input
                                placeholder='Input location...'
                                spellCheck='false'
                                onChange={(e) => setLocation(e.target.value)}
                                value={location}
                            />
                        </div>
                    </div>
                    <div className='row_input'>
                        <div className='col_input'>
                            <p>Participant *</p>
                            <input
                                placeholder='Input participant...'
                                spellCheck='false'
                                onChange={(e) => setParticipant(e.target.value)}
                                value={participant}
                            />
                        </div>
                        <div className='col_input'>
                            <p>Date *</p>
                            <input
                                placeholder='Input date...'
                                type='date'
                                onChange={(e) => setDate(e.target.value)}
                                value={date}
                            />
                        </div>
                    </div>
                    <div className='row_textarea'>
                        <p>Note *</p>
                        <textarea
                            placeholder='Input note...'
                            spellCheck='false'
                            onChange={(e) => setNote(e.target.value)}
                            value={note} />
                        <p className='note_required'>* Note must be more than 50 characters</p>
                    </div>
                    <div className='row_file'>
                        <input
                            placeholder='Input file...'
                            type='file'
                            onChange={changeHandler}
                            accept=".png,.jpg,.jpeg,.svg"
                        />
                    </div>
                    <div className='row_file'>
                        <button onClick={_submitHandler}>S U B M I T</button>
                    </div>

                </div>
                <div className='container_right'>
                    <div className='img' />
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = state => ({

});

const mapDispatchToProps = (dispatch => ({

}))();

export default connect(mapStateToProps, mapDispatchToProps)(AddEvent);