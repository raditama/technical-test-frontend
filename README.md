EVENT PROJECT
----------------------

* Versi Java = 1.8
* Database = H2
* Database name = event
* Database username = sa
* Database password =

Run Application
---------------

Client:
``` bash
# install node module
$ npm install

# running program
$ npm start
```

Service:
``` bash
# running program
$ mvn spring-boot:run
```
