package com.services.controllers;

import java.util.ArrayList;
import java.util.List;

import com.services.models.Event;
import com.services.repositories.EventRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/event")
public class EventController {

    @Autowired
    EventRepo eventRepo;

    @GetMapping("/read")
    public ResponseEntity<List<Event>> readAllEvent() {
        try {
            List<Event> event = new ArrayList<Event>();

            eventRepo.findAll().forEach(event::add);

            if (event.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(event, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Event> createEvent(@RequestBody Event event) {
        try {
            Event _event = eventRepo.save(new Event(event.getTitle(), event.getLocation(), event.getDate(),
                    event.getParticipant(), event.getNote(), event.getImage()));
            return new ResponseEntity<>(_event, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
