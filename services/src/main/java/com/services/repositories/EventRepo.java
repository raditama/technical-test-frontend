package com.services.repositories;

import com.services.models.Event;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EventRepo extends JpaRepository<Event, Integer> {
    
}
